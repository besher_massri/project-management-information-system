#This directory should include the env file
# Please copy the content below into a new file called ".env" and fill in the needed information


```bash
NODE_ENV=development|production
PORT=port_number

# mongodb configurations
MONGO_DB_HOST_NAME=host
MONGO_DB_HOST_PORT=port
MONGO_DB_DATABASE_NAME=database_name



# session & login secret keys
JWT_SECRET=jwt-secret
JWT_AUTH_HASH_COST=1
EXPRESS_SESSION_SECRET=session-secret


# team leader user credentionals
TEAM_LEADER_USERNAME=team-leader-user-name
TEAM_LEADER_PASSWORD=team-leader-password

```