const path = require('path');

// import configured node variables
require('dotenv').config({path: path.resolve(__dirname, '.env')});

// get process environment
const env = process.env.NODE_ENV || 'development';

// the common configurations
const environmentVars = {
  environment: env,
  port: process.env.PORT,
  mongodb: {
    hostName: process.env.MONGO_DB_HOST_NAME,
    hostPort: process.env.MONGO_DB_HOST_PORT,
    dbName: process.env.MONGO_DB_DATABASE_NAME
  },
  loginSecrets: {
    jwtSecret: process.env.JWT_SECRET,
    jwtAuthCost: +process.env.JWT_AUTH_HASH_COST,
    expressSessionSecret: process.env.EXPRESS_SESSION_SECRET
  },
  rootCredentials: {
    username: process.env.TEAM_LEADER_USERNAME,
    password: process.env.TEAM_LEADER_PASSWORD
  },
};
// export the configuration
module.exports = environmentVars;
