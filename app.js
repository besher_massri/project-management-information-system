var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var {expressLogger} = require('./logger');

var indexRouter = require('./controllers/index');
var {userAuthenticationRouter, sessionChecker} = require('./controllers/userAuthentication');
var usersRouter = require('./controllers/users');
var projectsRouter = require('./controllers/projects');
var tasksRouter = require('./controllers/tasks');
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(expressLogger);
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(userAuthenticationRouter, sessionChecker, indexRouter);
app.use(userAuthenticationRouter, sessionChecker, usersRouter);
app.use(userAuthenticationRouter, sessionChecker, projectsRouter);
app.use(userAuthenticationRouter, sessionChecker, tasksRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  res.render('notfound', {title: '404', user: req.session.user});
});
module.exports = app;
