const {createLogger, format, transports} = require('winston');
var expressWinston = require('express-winston');
const {ms, json, combine, timestamp, printf, colorize} = format;
const operationFormat = printf(({level, message, operation, timestamp, ms}) => {
  return `${timestamp} [${operation}] ${level}: ${message} -  ${ms}`;
});
const userFormat = printf(({level, message, user, timestamp, ms}) => {
  return `${timestamp} [${user}] ${level}: ${message} -  ${ms}`;
});

const expressFormat = printf(({level, meta, message, timestamp, req}) => {
  let method = meta.req.method;
  let responseTime = meta.responseTime;
  let status = meta.res.statusCode;
  return `${timestamp} [${method}][${status}][${responseTime} ms] ${level}: ${message}`;
});

function UserLogger(user) {
  return createLogger({
    format: combine(
      timestamp(),
      json(),
    ),
    defaultMeta: {user},
    transports: [
      new transports.File({filename: 'logs/error.log', level: 'error'}),
      new transports.File({filename: 'logs/combined.log'}),
      new transports.Console({
        format: combine(
          timestamp(),
          ms(),
          colorize(),
          userFormat
        )
      })
    ]
  });
}

function OperationLogger(operation) {
  return createLogger({
    format: combine(
      timestamp(),
      json(),
    ),
    defaultMeta: {operation},
    transports: [
      new transports.File({filename: 'logs/error.log', level: 'error'}),
      new transports.File({filename: 'logs/combined.log'}),
      new transports.Console({
        format: combine(
          timestamp(),
          ms(),
          colorize(),
          operationFormat
        )
      })
    ]
  });
}

let expressLogger = expressWinston.logger({
  transports: [
    new transports.Console({
      format: combine(
        timestamp(),
        colorize(),
        expressFormat
      )
    }),
    new transports.File({filename: 'logs/error.log', level: 'error'}),
    new transports.File({filename: 'logs/combined.log'}),
  ],
  format: format.combine(
    format.json()
  ),
  meta: true, // optional: control whether you want to log the meta data about the request (default to true)
  msg: "HTTP {{req.method}} {{req.url}}", // optional: customize the default logging message. E.g. "{{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}"
  expressFormat: true, // Use the default Express/morgan request formatting. Enabling this will override any msg if true. Will only output colors with colorize set to true
  colorize: true, // Color the text and status code, using the Express/morgan color palette (text: gray, status: default green, 3XX cyan, 4XX yellow, 5XX red).
  ignoreRoute: function (req, res) {
    return false;
  } // optional: allows to skip some log messages based on request and/or response
});

const logger = OperationLogger('Default');

module.exports = {logger, OperationLogger, UserLogger, expressLogger};
