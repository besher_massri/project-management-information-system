# Simple Project Management Information System

This project was done as a project assignment for Information Systems course at the University of Primorska

The system was implemented as an MVC system using node.js + express.js for handling the models and controller and ejs embedding engine for rending the views. It also uses mongodb as a database. 

## Description of the functionality

-	Three types of users: Team Leader (only one, defined in the configuration file), Project Managers, Developers
-	Adding/deleting projects to the team to work on. (team leader only)
-	Adding tasks to projects (editing projects) (project manager only)
-	Complete tasks and send them to review (editing tasks) (developer only)
-	Review tasks and approve the work. (project manager only)
-	Provide set of tasks for each developer, with ability to search and filter (content search/filtering) (developer only)
-	Export the displayed data in JSON/CSV format when appropriate. (all)
-	Provide summary page about the project (list of tasks with their status) (team leader + the project’s manager) 

## Usage

To use the system:

First clone the repository:

```$xslt
https://gitlab.com/besher_massri/project-management-information-system.git
```

Then enter the directory and install dependencies

```$xslt
cd project-management-information-system
npm install
```

After downloading run the program by typing

```$xslt
npm start
```

## Requirements

```$xslt
Node.js >= 12.10
mongodb >= 3.6
```


## Licence
MIT