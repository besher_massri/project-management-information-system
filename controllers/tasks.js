var express = require('express');
var router = express.Router();
var TasksModel = require('../models/TasksModel');
var ProjectsModel = require('../models/ProjectsModel');
var UsersModel = require('../models/UsersModel');
const {developerChecker, projectManagerChecker, atMostPMChecker} = require('./userAuthentication');
// route for user logout
router.get('/tasks', developerChecker, (req, res) => {
  let user = req.session.user;

  TasksModel.getTasks(undefined, user.username, function (data) {
    res.render('tasks.ejs', {title: 'Developer Tasks', user, data: JSON.stringify(data)});
  });
});

router.put('/change-task-status', atMostPMChecker, (req, res) => {
  let user = req.session.user;
  let taskId = req.body.taskId;
  let status = req.body.status;
  TasksModel.changeTaskStatus(taskId, status, user.username, function (ans) {
    res.json(ans);
  });
});

router.route('/add-task')
  .get(projectManagerChecker, (req, res) => {
    let user = req.session.user;
    let project = req.query.project;
    UsersModel.getUsers('developer', function (data) {
      data = data.map(cur => cur.username);
      res.render('addTask.ejs', {title: 'Add Task', user, project, developers: data});
    });
  }).post(projectManagerChecker, (req, res) => {
  let user = req.session.user;
  var name = req.body.name,
    description = req.body.description || "",
    project = req.body.project,
    developer = req.body.developer;
  TasksModel.createTask(name, description, project, developer, user.username, function (user) {
    if (user.error) {
      res.redirect('/add-task?completed=false&project=' + project)
    } else {
      res.redirect('/add-task?completed=true&project=' + project);
    }
  });
});
module.exports = router;
