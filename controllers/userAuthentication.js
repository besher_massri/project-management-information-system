var express = require('express');
var router = express.Router();
var session = require('express-session');
const {loginSecrets, mongodb} = require('../config/config');
let UsersModel = require('../models/UsersModel');
var MongoDBStore = require('connect-mongodb-session')(session);
var store = new MongoDBStore({
  uri: 'mongodb://' + mongodb.hostName + ':' + mongodb.hostPort + '/' + mongodb.dbName,
  collection: 'sessions'
});
// Catch errors
store.on('error', function (error) {
  console.log(error);
});

// initialize express-session to allow us track the logged-in user across sessions.
router.use(session({
  key: 'user_sid',
  secret: loginSecrets.expressSessionSecret,
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 360000000
  },
  store: store
}));


// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
  if (req.cookies.user_sid && (!req.session || !req.session.user)) {
    res.clearCookie('user_sid');
  }
  let url = req.url.indexOf('?') !== -1 ? req.url.slice(0, req.url.indexOf('?')) : req.url;
  if (url === '/login') {
    next();
    return;
  }
  if (req.cookies.user_sid) {
    next();
  } else {
    res.redirect('/login');

  }
};
// route for Home-Page
router.get('/', sessionChecker, (req, res) => {
  console.log('accessed main page');
  res.redirect('/dashboard');
});

function getCurrentSessionUser(req) {
  return req.session.user;
}

router.get('/get-user-status', (req, res) => {
  if (req.session.user) {
    res.status(200).json(req.session.user)
  } else {
    res.status(200).json({error: 'unauthorized'})
  }
});

let developerChecker = function (req, res, next) {
  const user = req.session.user;
  if (!user) {
    res.status(400).redirect('/unauthorized');
  } else {
    userAssign(req, res, next);
  }
};
let atLeastPMChecker = function (req, res, next) {
  const user = req.session.user;
  if (!user || (user.type !== 'project-manager' && user.type !== 'team-leader')) {
    res.status(400).redirect('/unauthorized');
  } else {
    userAssign(req, res, next);
  }
};
let atMostPMChecker = function (req, res, next) {
  const user = req.session.user;
  if (!user || (user.type !== 'project-manager' && user.type !== 'developer')) {
    res.status(400).redirect('/unauthorized');
  } else {
    userAssign(req, res, next);
  }
};

let projectManagerChecker = function (req, res, next) {
  const user = req.session.user;
  if (!user || user.type !== 'project-manager') {
    res.status(400).redirect('/unauthorized');
  } else {
    userAssign(req, res, next);
  }
};

let teamLeaderChecker = function (req, res, next) {
  const user = req.session.user;

  if (!user || user.type !== 'team-leader') {
    res.status(400).redirect('/unauthorized');
  } else {
    userAssign(req, res, next);
  }
};
let userAssign = function (req, res, next) {
  if (req.query) {
    req.query.user = req.user;
  }
  if (req.body) {
    req.body.user = req.user;
  }
  next();
};

// route for user signup
router.route('/add-user')
  .get(teamLeaderChecker, (req, res) => {
    res.render('signup.ejs', {title: 'Add User', user: req.session.user});
  })
  .post(userAssign, (req, res) => {
    var username = req.body.username,
      password = req.body.password,
      type = req.body.type;

    UsersModel.createUser(username, password, type, function (user) {
      if (user.error) {
        res.redirect('/add-user?completed=false');
      } else {
        res.redirect('/add-user?completed=true');
      }
    });
  });

// route for user Login
router.route('/login')
  .get((req, res) => {
    res.render('login.ejs', {title: 'Login', failed: true});
  })
  .post((req, res) => {
    var username = req.body.username,
      password = req.body.password;
    UsersModel.verifyUser(username, password, function (user) {
      if (user.error) {
        res.redirect('/login?failed=true')
      } else {
        req.session.user = user;
        //j.setCookie('jwt=' + req.session.user.jwt, common_api);
        res.redirect('/dashboard');
      }
    });
  });

// route for user logout
router.get('/logout', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.clearCookie('user_sid');
    res.redirect('/');
  } else {
    res.redirect('/login');
  }
});

function nxt(err) {
  if (err) {
    console.log(err);
  }
}

module.exports = {
  userAuthenticationRouter: router,
  getCurrentSessionUser,
  atMostPMChecker,
  atLeastPMChecker,
  developerChecker,
  teamLeaderChecker,
  projectManagerChecker,
  sessionChecker
};
