var express = require('express');
var router = express.Router();
var ProjectsModel = require('../models/ProjectsModel');
var UsersModel = require('../models/UsersModel');
const {atLeastPMChecker, teamLeaderChecker} = require('./userAuthentication');
// route for user logout
router.get('/projects', atLeastPMChecker, (req, res) => {
  let user = req.session.user;
  let pm = user.type === 'team-leader' ? undefined : user.username;
  ProjectsModel.getProjects(pm, function (data) {
    res.render('projects.ejs', {title: 'Projects', user, data: JSON.stringify(data)});
  });
});

router.route('/add-project')
  .get(teamLeaderChecker, (req, res) => {
    let user = req.session.user;
    UsersModel.getUsers('project-manager', function (data) {
      data = data.map(cur => cur.username);
      res.render('addProject.ejs', {title: 'Add Project', user, projectManagers: data});
    });
  }).post(teamLeaderChecker, (req, res) => {
  var name = req.body.name,
    description = req.body.description || "",
    projectManager = req.body.projectManager;
  ProjectsModel.createProject(name, description, projectManager, function (user) {
    if (user.error) {
      res.redirect('/add-project?completed=false')
    } else {
      res.redirect('/add-project?completed=true');
    }
  });
});

router.get('/project', atLeastPMChecker, (req, res) => {
  let user = req.session.user;
  let project = req.query.project;
  if (!project) {
    res.render('notfound', {title: '404', user});
    return;
  }
  ProjectsModel.getProject(project, user, function (data) {
    if (data.error) {
      res.render('unauthorized', {title: 'unauthorized', user});
      return;
    }
    res.render('projectDetail.ejs', {
      title: 'Project: ' + data.name,
      name: data.name,
      description: data.description,
      projectManager: data.projectManager,
      user,
      data: JSON.stringify(data.tasks)
    });
  });
});
// route for user logout
router.delete('/delete-project', teamLeaderChecker, (req, res) => {
  let user = req.session.user;
  let project = req.body.project;
  ProjectsModel.deleteProject(project, function (ans) {
    res.json(ans);
  });
});
module.exports = router;
