var express = require('express');
var router = express.Router();
var UsersModel = require('../models/UsersModel');
const {teamLeaderChecker} = require('./userAuthentication');
// route for user logout
router.get('/users', teamLeaderChecker, (req, res) => {
  let user = req.session.user;
  UsersModel.getUsers(undefined, function (data) {
    res.render('users.ejs', {title: 'Users',user, data: JSON.stringify(data)});
  });
});

module.exports = router;
