var express = require('express');
var router = express.Router();
/* GET home page. */
router.get('/unauthorized', function (req, res, next) {
  res.render('unauthorized', {title: 'Unauthorized', user: req.session.user});
});

// route for user's dashboard
router.get('/dashboard', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    let user = req.session.user;
    if (user.type === 'developer') {
      res.render('DeveloperDashboard.ejs', {title: 'Developer Dashboard', user});
    } else if (user.type === 'project-manager')
      res.render('projectManagerDashboard.ejs', {title: 'Project Manager Dashboard', user});
    else if (user.type === 'team-leader') {
      res.render('teamLeaderDashboard.ejs', {title: 'Team Leader Dashboard', user});
    } else {
      res.redirect('/login');
    }
  } else {
    res.redirect('/login');
  }
});

module.exports = router;
