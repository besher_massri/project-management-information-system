let dbManager = require("./db/DatabaseManager");
const {OperationLogger} = require('../logger');
let logger = OperationLogger('ProjectModel');
let BaseModel = require('./BaseModel');
const bcrypt = require('bcrypt');

class ProjectModel extends BaseModel {
  constructor() {
    super();
  }

  async _getProjectManager(projectManager) {
    const User = dbManager.getDBModels().User;
    if (projectManager && typeof projectManager === 'string') {
      return await User.findOne({username: projectManager}).exec();
    }
    return null;
  }

  async getProjects(projectManager, callback) {
    try {
      let self = this;
      logger.info("Retrieving projects");
      const Project = dbManager.getDBModels().Project;
      let config = {};
      let pm = await self._getProjectManager(projectManager);
      if (pm) {
        config['projectManager'] = pm.id;
      }
      let projects = await Project.find(config).exec();
      projects = projects.map(cur => cur.toJSON());
      const User = dbManager.getDBModels().User;
      for (const cur of projects) {
        try {
          cur.projectManager = (await User.findOne({_id: cur.projectManager}).exec()).username;
        } catch (e) {
          cur.projectManager = "";
        }
      }
      dbManager.clearHiddenParameters(projects);
      return this.returnDataShrt(projects, callback);
    } catch (e) {
      logger.info("Error in retrieving projects:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  }

  async createProject(name, description, projectManager, callback) {
    try {
      let self = this;
      logger.info("Creating project:\t" + name + " with manager:\t" + projectManager);
      let pm = await self._getProjectManager(projectManager);
      if (!pm) {
        return this.returnDataShrt({error: "No Project manager exists with the name:\t" + projectManager}, callback);
      }
      let model = dbManager.getDBModels().Project;
      const project = new model({
        name: name,
        description: description,
        projectManager: pm.id
      });
      await project.save();
      logger.info(`Project [${name}]Created`);
      return this.returnDataShrt(project, callback);
    } catch (e) {
      logger.info("Error in creating project:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  };

  async deleteProject(name, callback) {
    try {
      logger.info("Deleting project:\t" + name);
      await dbManager.getDBModels().Project.deleteOne({name: name});
      logger.info(`Project [${name}] Deleted`);
      return this.returnDataShrt({}, callback);
    } catch (e) {
      logger.info("Error in deleting project:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  }

  async getProject(name, user, callback) {
    try {
      let self = this;
      logger.info("Retrieving projects");
      const Project = dbManager.getDBModels().Project;
      let project = await Project.findOne({name: name}).exec();
      if (!project) {
        return this.returnDataShrt({error: "No project found with the name " + name}, callback);
      }
      let pm = await this._getProjectManager(user.username);
      if (!pm || (user.type !== 'team-leader' && project.projectManager.toString() !== pm.id)) {
        return this.returnDataShrt({error: "Unauthorized user for the project:\t" + name}, callback);
      }
      let prpm = await dbManager.getDBModels().User.findOne({_id: project.projectManager}).exec();
      let tasks = await dbManager.getDBModels().Task.find({project: project.id}).exec();
      tasks = tasks.map(cur => cur.toJSON());
      tasks.forEach(cur => cur.id = cur._id.toString());
      const User = dbManager.getDBModels().User;
      for (const cur of tasks) {
        try {
          cur.developer = (await User.findOne({_id: cur.developer}).exec()).username;
          cur.project = (await Project.findOne({_id: cur.project}).exec()).name;
        } catch (e) {
          cur.developer = "";
          cur.project = "";
        }
      }

      project = project.toJSON();
      if (prpm) {
        project.projectManager = prpm.username;
      }
      project.tasks = tasks;
      dbManager.clearHiddenParameters(project);
      return this.returnDataShrt(project, callback);
    } catch (e) {
      logger.info("Error in retrieving projects:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  }
}

module.exports = new ProjectModel();
