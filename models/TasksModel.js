let dbManager = require("./db/DatabaseManager");
const {OperationLogger} = require('../logger');
let logger = OperationLogger('TasksModel');
let BaseModel = require('./BaseModel');
const bcrypt = require('bcrypt');

class TasksModel extends BaseModel {
  constructor() {
    super();
  }

  async _getUserByName(developer) {
    const User = dbManager.getDBModels().User;
    if (developer && typeof developer === 'string') {
      return await User.findOne({username: developer}).exec();
    }
    return null;
  }

  async _getProject(project) {
    const Project = dbManager.getDBModels().Project;
    if (project && typeof project === 'string') {
      return await Project.findOne({name: project}).exec();
    }
    return null;
  }

  async getTasks(project, developer, callback) {
    try {
      let self = this;
      logger.info("Retrieving tasks");
      const Task = dbManager.getDBModels().Task;
      let config = {};
      let dev = await self._getUserByName(developer);
      if (dev) {
        config['developer'] = dev.id;
      }
      let pr = await self._getProject(project);
      if (pr) {
        config['project'] = pr.id;
      }
      let tasks = await Task.find(config).exec();
      tasks = tasks.map(cur => cur.toJSON());
      tasks.forEach(cur => cur.id = cur._id.toString());
      const User = dbManager.getDBModels().User;
      const Project = dbManager.getDBModels().Project;
      for (const cur of tasks) {
        try {
          cur.developer = (await User.findOne({_id: cur.developer}).exec()).username;
          cur.project = (await Project.findOne({_id: cur.project}).exec()).name;
        } catch (e) {
          cur.developer = "";
          cur.project = "";
        }
      }
      dbManager.clearHiddenParameters(tasks);
      return this.returnDataShrt(tasks, callback);
    } catch (e) {
      logger.info("Error in retrieving tasks:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  }

  async createTask(name, description, project, developer, projectManager, callback) {
    try {
      let self = this;
      logger.info("Creating project:\t" + name + " with manager:\t" + projectManager);
      let dev = await self._getUserByName(developer);
      if (!dev) {
        return this.returnDataShrt({error: "No developer exists with the name:\t" + developer}, callback);
      }
      let pm = await self._getUserByName(projectManager);
      if (!pm) {
        return this.returnDataShrt({error: "No project manager exists with the name:\t" + projectManager}, callback);
      }
      let pr = await self._getProject(project);
      if (!pr) {
        return this.returnDataShrt({error: "No project exists with the name:\t" + project}, callback);
      }
      if (pr.projectManager.toString() !== pm.id) {
        return this.returnDataShrt({error: "Unauthorized project manager:\t" + projectManager}, callback);
      }

      let model = dbManager.getDBModels().Task;
      const task = new model({
        name: name,
        description: description,
        developer: dev.id,
        project: pr.id,
        status: 'TODO'
      });
      await task.save();
      logger.info(`Task [${name}] Created`);
      return this.returnDataShrt(task, callback);
    } catch (e) {
      logger.info("Error in creating task:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  };

  async changeTaskStatus(taskId, status, user, callback) {
    try {
      let self = this;
      if (['TODO', 'In-review', 'Done'].indexOf(status) === -1) {
        return self.returnDataShrt({error: "Invalid status"}, callback);
      }
      logger.info("Change task status");
      const Task = dbManager.getDBModels().Task;
      let config = {};
      let task = await Task.findOne({_id: taskId}).exec();
      if (!task) {
        return self.returnDataShrt({error: "No task exists with the id:\t" + taskId}, callback);
      }
      let usr = await this._getUserByName(user);
      if (!usr) {
        return self.returnDataShrt({error: "No user with the name:\t" + user}, callback);
      }
      let project = await dbManager.getDBModels().Project.findOne({_id: task.project}).exec();
      if (!project) {
        logger.error("Invalid project stored");
        return self.returnDataShrt({error: "Internal error"}, callback);
      }
      if (usr.id !== project.projectManager.toString() && usr.id !== task.developer.toString()) {
        return self.returnDataShrt({error: "Unauthorized User"}, callback);
      }
      await Task.updateOne({_id: taskId}, {status: status}).exec();
      return this.returnDataShrt({}, callback);
    } catch (e) {
      logger.info("Error in changing task status:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  }
}

module.exports = new TasksModel();
