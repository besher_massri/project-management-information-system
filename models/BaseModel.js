class BaseModel {
  constructor() {
  }

  returnDataShrt(data, callback) {
    if (data.toJSON) {
      data = data.toJSON();
    }
    if (callback) {
      callback(data);
      return;
    }
    return data;
  };
}

module.exports = BaseModel;
