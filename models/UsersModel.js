const {loginSecrets, rootCredentials} = require('../config/config');
let dbManager = require("./db/DatabaseManager");
const {OperationLogger} = require('../logger');
let logger = OperationLogger('UsersModel');
let BaseModel = require('./BaseModel');
const bcrypt = require('bcrypt');

class UsersModel extends BaseModel {
  constructor() {
    super();
    let self = this;
    setTimeout(function () {
      this.model = dbManager.getDBModels().User;
      self._initData();
    }, 2000);
  }

  async getUsers(type, callback) {
    const User = dbManager.getDBModels().User;
    let config = {};
    if (type && ['developer', 'project-manager', 'team-leader'].indexOf(type) !== -1) {
      config['type'] = type;
    }
    let users = await User.find(config).exec();
    users = users.map(cur => cur.toJSON());
    dbManager.clearHiddenParameters(users);
    users.forEach(cur => {
      delete cur.passwordHash;
    });
    return this.returnDataShrt(users, callback);
  }

  async verifyUser(username, password, callback) {
    const User = dbManager.getDBModels().User;
    const userDocument = await User.findOne({username: username}).exec();
    if (!userDocument) {
      logger.info(`[${username}]: Authentication unsuccessful`);
      callback({error: "Authentication unsuccessful"});
      return;
    }
    const passwordsMatch = await bcrypt.compare(password, userDocument.passwordHash);
    if (passwordsMatch) {
      logger.info(`[${username}]: Authentication successful`);
      callback({username: userDocument.username, type: userDocument.type});
    } else {
      logger.info(`[${username}]: Authentication unsuccessful`);
      callback({error: "Authentication unsuccessful"});
    }
  }

  async createUser(username, password, type, callback) {
    try {
      logger.info("Creating user:\t" + username);
      const passwordHash = await bcrypt.hash(password, loginSecrets.jwtAuthCost);
      let model = dbManager.getDBModels().User;
      const userDocument =  new model({
        username: username,
        passwordHash: passwordHash,
        type: type
      });
      await userDocument.save();
      logger.info(`User [${username}]Added`);
      return this.returnDataShrt(userDocument, callback);
    } catch (e) {
      logger.info("Error in creating user:\t" + e.message);
      return this.returnDataShrt({error: e.message}, callback);
    }
  };

  async isRoot(username, callback) {
    let res = username === rootCredentials.username && await dbManager.getDBModels().User.countDocuments({username: username}) > 0;
    return this.returnDataShrt(res, callback);
  }

  async _initData() {
    let self = this;
    let models = dbManager.getDBModels();
    // create the root user if it does not exist
    if ((await models.User.countDocuments({username: rootCredentials.username})) === 0) {
      await self.createUser(rootCredentials.username, rootCredentials.password, "team-leader", null);
    }
    if (!await self.isRoot(rootCredentials.username)) {
      logger.error('error in adding root user');
    }
  }
}

module.exports = new UsersModel();
