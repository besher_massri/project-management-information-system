let mongoose = require('mongoose');
let uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let UserSchema = new Schema({
  username: {type: String, required: true, index: true, unique: true},
  type: {type: String, enum: ['developer', 'project-manager', 'team-leader'], required: true},
  passwordHash: { //salted and hashed using bcrypt
    type: String,
    required: true
  }
});

UserSchema.plugin(uniqueValidator);

module.exports = UserSchema;
