let mongoose = require('mongoose');
const {mongodb} = require('../../config/config');
let {OperationLogger, UserLogger} = require('../../logger');
const DBLogger = OperationLogger("DATABASE");
mongoose.set('useCreateIndex', true);

let UserSchema = require("./UserSchema");
let ProjectSchema = require("./ProjectSchema");
let TaskSchema = require("./TaskSchema");

class DatabasesManager {
  constructor() {
    let self = this;
    self.done = false;
    self.database = mongoose.createConnection('mongodb://' + mongodb.hostName + ':' + mongodb.hostPort + '/' + mongodb.dbName, {useNewUrlParser: true});


    self.database.on('error', console.error.bind(console, 'admin DB connection error'));
    self.database.once('open', async function () {
      DBLogger.info("Database is connected");
      DBLogger.debug("Connecting models");
      let models = self._connectModels(self.database);
      DBLogger.debug("Filling init data");
      await self._initData(models);
      self.logger = UserLogger("Default");
      self.userLoggers = {};
      DBLogger.info("Database is loaded");
      self.done = true;
    });
  }

  async untilDone() {
    while (true) {
      if (this.done) {
        return;
      }
      await null; // prevents app from hanging
    }
  }

  async _initData(models) {
    //add init data
  }

  _connectModels(conn) {
    let models = {};
    models.User = conn.model('User', UserSchema);
    models.Project = conn.model('Project', ProjectSchema);
    models.Task = conn.model('Task', TaskSchema);
    return models;
  }

  getDBModels() {
    return this.database.models;
  }


  getUserLogger(username) {
    if (!this.userLoggers.hasOwnProperty(username)) {
      this.userLoggers[username] = {logger: this.logger.child({user: username})};
    }
    return this.userLoggers[username].logger;
  }

  clearHiddenParameters(obj) {
    let self = this;
    if (obj === null || obj === undefined) {
      return;
    }
    if (Array.isArray(obj)) {
      obj.forEach(function (item) {
        self.clearHiddenParameters(item);
      });
      return;
    }
    Object.keys(obj).forEach(function (key) {
      if (key.startsWith('_')) {
        delete obj[key];
      } else if (typeof (obj[key]) === 'object') {
        self.clearHiddenParameters(obj[key]);
      }
    });
  }
}

const dbMananger = module.exports = new DatabasesManager();
