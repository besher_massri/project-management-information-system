let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let TaskSchema = new Schema({
  name: {type: String, required: true},
  description: {type: String},
  developer: {type: Schema.Types.ObjectId, ref: 'User', index: true},
  project: {type: Schema.Types.ObjectId, ref: 'Project', index: true},
  status: {type: String, enum: ['TODO', 'In-review', 'Done'], default: 'TODO', index: true}
});

module.exports = TaskSchema;
