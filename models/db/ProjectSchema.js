let mongoose = require('mongoose');
let uniqueValidator = require('mongoose-unique-validator');
let Schema = mongoose.Schema;

let ProjectSchema = new Schema({
  name: {type: String, required: true, index: true, unique: true},
  description: {type: String},
  projectManager: {type: Schema.Types.ObjectId, ref: 'User'}
});

ProjectSchema.plugin(uniqueValidator);

module.exports = ProjectSchema;
